<?php
/**
 * Created by PhpStorm.
 * User: kemal.yenilmez
 * Date: 9/12/2017
 * Time: 7:57 AM
 */

use Lcobucci\JWT\Signer\Hmac\Sha256;

$route = new League\Route\RouteCollection($container);

$route->group('/auth', function ($route) {
    $route->map('POST', '/register', 'Vacuum\Controllers\AuthController::register');
    $route->map('POST', '/login', 'Vacuum\Controllers\AuthController::login');
});


$route->map('GET', '/', 'Vacuum\Controllers\HomeController::index');
$route->map('POST', '/', 'Vacuum\Controllers\LogController::store')->middleware(
    new \Mvdstam\PhpJwtMiddleware\JwtMiddleware(
        new \Mvdstam\PhpJwtMiddleware\Services\JwtVerificationService(new Sha256(), 'testing'),
        new \Mvdstam\PhpJwtMiddleware\Services\JwtProvider()
    ));

$route->map('GET', '/token', 'Vacuum\Controllers\HomeController::getToken');

$route->group("/", function ($route) {
       $route->map('GET', '/secret', 'Vacuum\Controllers\HomeController::secret');
        $route->map('GET', '/books', 'Vacuum\Controllers\HomeController::books');
        $route->map('GET', '/books/{book:number}/show', 'Vacuum\Controllers\HomeController::book')->middleware(new Vacuum\Middlewares\BooksMiddleware());
    })
    ->middleware(
        new \Mvdstam\PhpJwtMiddleware\JwtMiddleware(
            new \Mvdstam\PhpJwtMiddleware\Services\JwtVerificationService(new Sha256(), 'testing'),
            new \Mvdstam\PhpJwtMiddleware\Services\JwtProvider()
    ));

return $route;