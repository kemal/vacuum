<?php
/**
 * Created by PhpStorm.
 * User: kemal.yenilmez
 * Date: 9/12/2017
 * Time: 7:56 AM
 */


$container = new League\Container\Container;

$container->delegate(
    new \League\Container\ReflectionContainer
);

$container->add('authService', \Vacuum\Services\AuthService::class);

$container->addServiceProvider(\Vacuum\Providers\ServiceProvider::class);

$container->inflector(Vacuum\Controllers\BaseController::class)
    ->invokeMethod('setBookService', ['BookService']);


$container->add('Apix\Log\Logger\File', function () {
    return new Apix\Log\Logger\File(__DIR__ . '/logs/app.log');
});

$container->share('response', Zend\Diactoros\Response::class);
$container->share('request', function () {
    return Zend\Diactoros\ServerRequestFactory::fromGlobals(
        $_SERVER, $_GET, $_POST, $_COOKIE, $_FILES
    );
});

$container->share('emitter', Zend\Diactoros\Response\SapiEmitter::class);
return $container;