<?php

require "config/database.php";
$container = require "di.php";
$route = require "routes.php";

use Vacuum\Models\Database;
new Database();

$response = $route->dispatch($container->get('request'), $container->get('response'));
$container->get('emitter')->emit($response);




