<?php

namespace Vacuum\Middlewares;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class BooksMiddleware
{
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next)
    {
        //$response->getBody()->write('This will run before your controller.');
        $response = $next($request, $response);
        //$response->getBody()->write('This will run after your controller.');
        return $response;
    }
}