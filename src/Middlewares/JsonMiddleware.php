<?php

namespace Vacuum\Middlewares;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class JsonMiddleware
{
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next)
    {
        $data = json_decode((string) $request->getBody());
        $request = $request->withParsedBody($data);
        $response = $next($request, $response);
        return $response;
    }
}