<?php

namespace Vacuum\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use Vacuum\Transformers\BookTransformer;
use League\Fractal;
use Vacuum\Models\Book;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class AuthController extends BaseController
{
    public function register(Request $request, Response $response)
    {
        $data = json_decode($request->getBody()->getContents());
        $this->authService->register($data->email, $data->password);
        $token = $this->authService->createToken();

        $response->getBody()->write(json_encode(['token' => $token]));
        return $response->withHeader(
            'Content-Type',
            'application/json'
        )->withStatus(200);
    }

    public function login(Request $request, Response $response)
    {
        $data = json_decode($request->getBody()->getContents());
        $this->authService->login($data->email, $data->password);
        $token = $this->authService->createToken();

        $response->getBody()->write(json_encode(['token' => $token]));
        return $response->withHeader(
            'Content-Type',
            'application/json'
        )->withStatus(200);
    }

    /*    public function login(Request $request, Response $response)
        {
            $token = $request->getAttribute('jwt');
            $token = (new Parser())->parse((string) $token);
            var_dump($token->getClaim("name")); // Retrieves the token header);
            die();
            $response->getBody()->write($token);
            return $response->withHeader(
                'Content-Type',
                'application/json'
            )->withStatus(200);*/


}