<?php
/**
 * Created by PhpStorm.
 * User: kemal.yenilmez
 * Date: 9/12/2017
 * Time: 7:52 AM
 */

namespace Vacuum\Controllers;

use Apix\Log\Logger\File;
use Vacuum\Services\AuthService;
use Vacuum\Services\BookService;


class BaseController
{
    protected $authService;
    protected $log;
    protected  $bookService;
    public function __construct(AuthService $authService, File $logger)
    {
        $this->authService = $authService;
        $this->log = $logger;
    }

    public function setBookService(BookService $bookService){
        $this->bookService = $bookService;
    }

    public function getBookService(){
        return $this->bookService;
    }
}