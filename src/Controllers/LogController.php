<?php

namespace Vacuum\Controllers;


use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use League\Fractal\Manager;
use Vacuum\Services\AuthService;
use Vacuum\Transformers\BookTransformer;
use League\Fractal;
use Vacuum\Models\Log;

class LogController extends BaseController
{
    public function store(Request $request, Response $response)
    {
        $company = $this->authService->getCompany($request);
        $params = $request->getParsedBody();
        Log::create(['channel' =>  $params['channel'], 'company_id' => $company->id, 'context' => $params['context'], 'level' => $params['level'], 'level_name' => $params['level_name']]);
        $response->getBody()->write(json_encode(["hi" => $company->name, 'message' => $params['message']]));
        return $response->withHeader(
            'Content-Type',
            'application/json'
        )->withStatus(200);
    }
}