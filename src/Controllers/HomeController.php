<?php

namespace Vacuum\Controllers;


use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use League\Fractal\Manager;
use Vacuum\Services\AuthService;
use Vacuum\Transformers\BookTransformer;
use League\Fractal;
use Vacuum\Models\Book;

class HomeController extends BaseController
{
    public function index(Request $request, Response $response)
    {

        $name = $this->getBookService()->sayHello();

        $response->getBody()->write(json_encode(["hi" => $name]));
        return $response->withHeader(
            'Content-Type',
            'application/json'
        )->withStatus(200);
    }

    public function secret(Request $request, Response $response)
    {
        $user = $this->authService->getUser($request);

        $response->getBody()->write(json_encode(["secret name" => $user->email]));
        return $response->withHeader(
            'Content-Type',
            'application/json'
        )->withStatus(200);
    }

    public function books(Request $request, Response $response)
    {
        $fractal = new Manager();

        $books = Book::all();
        $resource = new Fractal\Resource\Collection($books, new BookTransformer);
        $response->getBody()->write($fractal->createData($resource)->toJson());

        return $response->withHeader(
            'Content-Type',
            'application/json'
        )->withStatus(200);
    }

    public function book(Request $request, Response $response, array $args)
    {
        $fractal = new Manager();

        $books = Book::find($args['book']);
        $resource = new Fractal\Resource\Item($books, new BookTransformer);

        $response->getBody()->write($fractal->createData($resource)->toJson());

        return $response->withHeader(
            'Content-Type',
            'application/json'
        )->withStatus(200);
    }

    public function getToken(Request $request, Response $response)
    {
        $token = $this->authService->createToken();
        $response->getBody()->write(json_encode(["token" => $token]));
        return $response->withHeader(
            'Content-Type',
            'application/json'
        )->withStatus(200);
    }
}