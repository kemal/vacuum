<?php
/**
 * Created by PhpStorm.
 * User: kemal.yenilmez
 * Date: 9/25/2017
 * Time: 5:11 PM
 */

namespace Vacuum\Models;


use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $table = "logs";
    protected $fillable = ['channel', 'company_id', 'context', 'level', 'level_name'];

 
}