<?php
/**
 * Created by PhpStorm.
 * User: kemal.yenilmez
 * Date: 9/15/2017
 * Time: 8:57 AM
 */

namespace Vacuum\Providers;

use League\Container\ServiceProvider\AbstractServiceProvider;
use Vacuum\Services\BookService;

class ServiceProvider extends AbstractServiceProvider
{
    protected $provides = [
        'BookService'
    ];

    /**
     * Use the register method to register items with the container via the
     * protected $this->container property or the `getContainer` method
     * from the ContainerAwareTrait.
     *
     * @return void
     */
    public function register()
    {
        // TODO: Implement register() method.
        $this->getContainer()->share('BookService', new BookService());
    }
}