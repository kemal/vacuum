<?php

namespace Vacuum\Services;

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use \Psr\Http\Message\ServerRequestInterface as Request;
use Vacuum\Exceptions\TokenNotFoundException;
use Vacuum\Models\Company;
use Vacuum\Models\User;

class AuthService
{
    protected $user;
    public function getCompany(Request $request)
    {
        $token = $request->getAttribute('jwt', null);
        if (!$token) throw new TokenNotFoundException();
        $token = (new Parser())->parse((string)$token);
        $company = Company::where('id', $token->getClaim("company_id"))->first();
        
        return $company;
    }

    public function createToken($company_id = 0)
    {
        $signer = new Sha256();
        $token = (new Builder())->setIssuer('http://example.com')// Configures the issuer (iss claim)
        ->setAudience('http://example.org')// Configures the audience (aud claim)
        ->setId('4f1g23a12aa', true)// Configures the id (jti claim), replicating as a header item
        ->setIssuedAt(time())// Configures the time that the token was issue (iat claim)
        ->setNotBefore(time() + 60)// Configures the time that the token can be used (nbf claim)
        ->setExpiration(time() + 3600)// Configures the expiration time of the token (exp claim)
        ->set('company_id', "1")// Configures a new claim, called "uid"
        ->set('email', 'kemal@gazatem.com')// Configures a new claim, called "uid"
        ->sign($signer, 'testing')// creates a signature using "testing" as key
        ->getToken(); // Retrieves the generated token

        return (string)$token;
    }
    
    public function getUser(Request $request)
    {
        $token = $request->getAttribute('jwt', null);
        if (!$token) throw new TokenNotFoundException();
        $token = (new Parser())->parse((string)$token);
        $user = User::where('email', $token->getClaim("email"))->first();
        $this->user = $user;
        return $user;
    }

    public function register($email, $password)
    {
        $this->user = User::create(['email' => $email, 'password' => $password]);
        return $this->user;
    }

    public function login($email, $password)
    {
        $user = User::where('email', $email)->where('password', $password)->first();
        $this->user = $user;
        return $user;
    }    
}